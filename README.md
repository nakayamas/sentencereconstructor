Running scripts are in a `script` directory.

If you want to run the sentence reconstructor program with ChaPAS, execute a following script

    script/reconstructor/all-method-chapas.sh

You can use a configuration file. A defalt configuration file is:

    script/runner.conf

I recommend to set parameters of `input-file` and `chapas-jar` at least.
