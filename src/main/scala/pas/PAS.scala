package pas

/**
  * Created by nakayama.
  */
case class PAS(
                  predicate: Token,
                  arguments: Map[ArgLabel.Value, Token]
              )