package pas

/**
  * Created by nakayama.
  */
case class Sentence(id: Int, chunks: Seq[Chunk]) {
    val tokens: Seq[Token] = chunks.flatMap(_.tokens)

    val text: String = chunks.map(_.text).mkString

    private var doc_ : Doc = _

    def doc: Doc = doc_

    def doc_=(doc: Doc) = doc_ = doc

    private var depTree_ : String = ""

    def depTree: String = depTree_

    def depTree_=(depTree: String) = depTree_ = depTree

    private var ntcText_ : String = ""

    def ntcText: String = ntcText_

    def ntcText_=(ntcText: String) = ntcText_ = ntcText

    lazy val pases: Seq[PAS] = chunks.flatMap(_.pases)

    override def toString = text
}
