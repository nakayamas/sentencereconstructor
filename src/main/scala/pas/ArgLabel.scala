package pas

/**
  * Created by nakayama.
  */
object ArgLabel extends Enumeration {
    val Ga, Wo, Ni = Value

    def extendedWithName(name: String): ArgLabel.Value =
        name match {
            case "Ga" | "ga" => Ga
            case "Wo" | "wo" | "o" => Wo
            case "Ni" | "ni" => Ni
        }

}