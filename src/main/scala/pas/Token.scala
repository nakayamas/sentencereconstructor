package pas

/**
  * Created by nakayama.
  */
case class Token(
                    id: Int,
                    surface: String,
                    base: String,
                    pos: String,
                    detailedPos: Option[String],
                    semantic: Option[String],
                    private val pasMap: Option[Map[String, String]]
                ) {
    private var doc_ : Doc = _

    def doc: Doc = doc_

    def doc_=(doc: Doc) = doc_ = doc

    private var sentence_ : Sentence = _

    def sentence: Sentence = sentence_

    def sentence_=(sentence: Sentence) = sentence_ = sentence

    private var chunk_ : Chunk = _

    def chunk: Chunk = chunk_

    def chunk_=(chunk: Chunk) = chunk_ = chunk

    lazy val prev: Option[Token] = sentence.tokens.lift(id - 1)

    lazy val next: Option[Token] = sentence.tokens.lift(id + 1)

    lazy val isPredicate: Boolean = pasMap match {
        case Some(map) if map.get("type").exists(e => e == "pred" || e == "noun") => true
        case _ => false
    }

    lazy val argId: Option[Int] = pasMap match {
        case Some(map) =>
            map.get("id") orElse map.get("ID") match {
                case Some(x) => Some(x.toInt)
                case None => None
            }
        case None => None
    }

    lazy val pas: Option[PAS] = pasMap match {
        case Some(map) =>
            val arguments = map.collect {
                case (labelName, argIdStr) if labelName.matches( """(ga|o|ni)""") =>
                    val argToken = doc.tokens.find(_.argId match {
                        case Some(x) if x == argIdStr.toInt => true
                        case _ => false
                    })
                    assume(argToken.isInstanceOf[Some[Token]], s"${this.surface} refers unknown argument ID: ${argIdStr}")
                    ArgLabel.extendedWithName(labelName) -> argToken.get
            }
            Some(PAS(this, arguments))
        case _ => None
    }

    override val toString = surface
}