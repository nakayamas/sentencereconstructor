package pas.parser

import config.SentenceUnitReconstructorConfig
import util.process.EchoProcess
import util.string.NormalizedStringOption

import scala.sys.process.{Process, ProcessBuilder}
import scala.util.matching.Regex

/**
  * Created by nakayama.
  */
object CaboCha extends EchoProcess {
    final private[this] val cabochaLatticeIPA: String = {
        SentenceUnitReconstructorConfig.cabochaLatticeIPA match {
            case Some(c) => c
            case _ => throw new RuntimeException("CaboCha lattice with IPA command was not found.")
        }
    }

    def cabochaLatticeIPACmd(sentence: NormalizedStringOption): ProcessBuilder =
        echo(sentence) #| Process(cabochaLatticeIPA)

    final private[this] val cabochaTreeIPA: String = {
        SentenceUnitReconstructorConfig.cabochaTreeIPA match {
            case Some(c) => c
            case _ => throw new RuntimeException("CaboCha tree with IPA command was not found.")
        }
    }

    def cabochaTreeIPACmd(sentence: NormalizedStringOption): ProcessBuilder =
        echo(sentence) #| Process(cabochaTreeIPA)

    final private[this] val cabochaLatticeJuman: String = {
        SentenceUnitReconstructorConfig.cabochaLatticeJuman match {
            case Some(c) => c
            case _ => throw new RuntimeException("CaboCha lattice with Juman command was not found.")
        }
    }

    def cabochaLatticeJumanCmd(sentence: NormalizedStringOption): ProcessBuilder =
        echo(sentence) #| Process(cabochaLatticeJuman)

    final private[this] val cabochaTreeJuman: String = {
        SentenceUnitReconstructorConfig.cabochaTreeJuman match {
            case Some(c) => c
            case _ => throw new RuntimeException("CaboCha tree with Juman command was not found.")
        }
    }

    def cabochaTreeJumanCmd(sentence: NormalizedStringOption): ProcessBuilder =
        echo(sentence) #| Process(cabochaTreeJuman)

    final val treeChunkRegExp: Regex = """(\s*)([^-D]*)(-*D?.*)""".r
}