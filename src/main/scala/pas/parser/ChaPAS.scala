package pas.parser

import config.SentenceUnitReconstructorConfig
import pas.Doc
import reader.ChaPASNtcParser
import util.ChaPASCache
import util.process.EchoProcess
import util.string.NormalizedStringOption

import scala.sys.process.Process

object ChaPAS extends PasAnalyzer with EchoProcess with ChaPASCache {
    private val chapasCmd: String = {
        SentenceUnitReconstructorConfig.chapas match {
            case Some(c) => c
            case _ => throw new RuntimeException("ChaPAS command was not found.")
        }
    }

    override def analyze(sentence: NormalizedStringOption): Stream[String] = {
        println(s"analyzing ...: [${sentence.get.toString.take(15).mkString}...]")
        echo(sentence).#|(Process(chapasCmd)).lineStream_!
    }

    override def cabochaLattice(sentence: NormalizedStringOption): Stream[String] =
        CaboCha.cabochaLatticeIPACmd(sentence).lineStream_!

    override def cabochaTree(sentence: NormalizedStringOption): Stream[String] =
        CaboCha.cabochaTreeIPACmd(sentence).lineStream_!

    override final protected[this] def textToDoc(normalizedSentencesOption: NormalizedStringOption): Doc = {
        (if (cacheEnable) getDocCache(normalizedSentencesOption) else None) getOrElse
            this.parser.parse(this.analyze(normalizedSentencesOption).iterator)
    }

    override val parser: ChaPASNtcParser.type = ChaPASNtcParser
}