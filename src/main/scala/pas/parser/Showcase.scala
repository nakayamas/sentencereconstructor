package pas.parser

import config.SentenceUnitReconstructorConfig
import pas.Doc
import reader.ShowcaseNtcParser
import util.ShowcaseCache
import util.process.EchoProcess
import util.string.NormalizedStringOption

import scala.sys.process.Process

object Showcase extends PasAnalyzer with EchoProcess with ShowcaseCache {
    private val showcaseCmd: String = {
        SentenceUnitReconstructorConfig.showcase match {
            case Some(c) => c
            case _ => throw new RuntimeException("Showcase command was not found.")
        }
    }

    override def analyze(sentence: NormalizedStringOption): Stream[String] =
        CaboCha.cabochaLatticeJumanCmd(sentence).#|(Process(showcaseCmd)).lineStream_!

    override def cabochaLattice(sentence: NormalizedStringOption): Stream[String] =
        CaboCha.cabochaLatticeJumanCmd(sentence).lineStream_!

    override def cabochaTree(sentence: NormalizedStringOption): Stream[String] =
        CaboCha.cabochaTreeJumanCmd(sentence).lineStream_!

    override final protected[this] def textToDoc(normalizedSentencesOption: NormalizedStringOption): Doc =
        (if (cacheEnable) getDocCache(normalizedSentencesOption) else None) getOrElse
            this.parser.parse(this.analyze(normalizedSentencesOption).iterator)

    override val parser: ShowcaseNtcParser.type = ShowcaseNtcParser
}