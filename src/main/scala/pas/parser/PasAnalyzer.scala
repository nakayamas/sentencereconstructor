package pas.parser

import pas.Doc
import reader.NtcParser
import util.Cache
import util.string.{NormalizedString, NormalizedStringOption, StringOption}

/**
  * Created by nakayama.
  */
abstract class PasAnalyzer extends Cache {
    def analyze(sentence: NormalizedStringOption): Stream[String]

    def cabochaLattice(sentence: NormalizedStringOption): Stream[String]

    def cabochaTree(sentence: NormalizedStringOption): Stream[String]

    protected[this] def textToDoc(normalizedSentencesOption: NormalizedStringOption): Doc

    val parser: NtcParser

    def parse(normalizedSentence: NormalizedString): Doc =
        textToDoc(NormalizedStringOption(normalizedSentence))

    def parse(normalizedSentences: Seq[NormalizedString]): Doc =
        parse(NormalizedString(StringOption(normalizedSentences.mkString("\n"))))
}
