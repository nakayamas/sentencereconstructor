package pas

/**
  * Created by nakayama.
  */
case class Doc(sentences: Seq[Sentence]) {
    val text: String = sentences.map(_.text).mkString("\n")

    val tokens: Seq[Token] = sentences.flatMap(_.tokens)

    lazy val pases: Seq[PAS] = sentences.flatMap(_.pases)

    override def toString = text
}