package reconstructor.unit

import pas.ArgLabel.{Ga, Ni, Wo}
import pas.PAS
import util.Color
import util.Color.{BLUE, RESET}

/**
  * Created by nakayama.
  */
final case class PAUnit(pas: PAS) {
    val representative: String = {
        val arguments = pas.arguments
        List((Ga, "が"), (Wo, "を"), (Ni, "に")).map { case (label, punc) =>
            arguments.get(label) match {
                case Some(t) => t.chunk.textWithoutParticlesAndPuncs + punc
                case _ => ""
            }
        }.mkString + pas.predicate.base
    }

    val coloredRepresentative: String = {
        val arguments = pas.arguments
        List((Ga, "が"), (Wo, "を"), (Ni, "に")).map { case (label, punc) =>
            arguments.get(label) match {
                case Some(t) => Color.coloring(t.chunk.tokensWithoutParticlesAndPuncs, pas) + punc
                case _ => ""
            }
        }.mkString + s"${BLUE}${pas.predicate.base}${RESET}"
    }
}