package reconstructor

import pas.Doc
import pas.parser.{ChaPAS, PasAnalyzer}
import reader.RawSentenceParser
import reconstructor.unit.CombineUnit
import util.string.{NormalizedString, StringOption}

/**
  * Created by nakayama.
  */
object SentenceUnitReconstructor {
    def main(args: Array[String]): Unit = {
        sentenceUnitReconstructorWithChaPAS(
            """ウェストファリア条約で帝国内の領邦がほぼ完全な主権を認められたことにより、一つの帝国としての機能は失われた。""")
            .foreach(println)
    }

    def sentenceUnitReconstructorWithChaPAS(text: String): Seq[String] = {
        val pasAnalyzer: PasAnalyzer = ChaPAS

        // Normalize sentences
        val normalizedSentences: Seq[NormalizedString] = RawSentenceParser.parse(StringOption(text))

        // Parse sentences
        val pasDocs: Seq[Doc] = normalizedSentences.map(pasAnalyzer.parse)

        // Generate combination units
        val filteredSentences: Seq[CombineUnit] =
            pasDocs.flatMap(_.sentences.map(new CombineUnit(_)))

        // Output
        filteredSentences.flatMap(_.representatives)
    }
}
