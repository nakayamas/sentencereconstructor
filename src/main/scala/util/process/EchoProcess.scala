package util.process

import util.string.{NormalizedStringNone, NormalizedStringOption, NormalizedStringSome}

import scala.sys.process.{Process, ProcessBuilder}

/**
  * Created by nakayama.
  */
trait EchoProcess {
    def echo(str: String): ProcessBuilder = Process(Seq("/usr/bin/printf", str.toString))

    def echo(sentence: NormalizedStringOption): ProcessBuilder = {
        sentence match {
            case NormalizedStringSome(s) => echo(s.toString)
            case NormalizedStringNone => Process(Nil)
        }
    }
}