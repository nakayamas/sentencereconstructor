package util

import java.io.{File, FileNotFoundException, PrintStream, PrintWriter}

import scala.io.Source

/**
  * Created by nakayama.
  */
object FileOps {
    def getFileFromDir(path: String): Array[File] = getFileFromDir(new File(path))

    def getFileFromDir(file: File): Array[File] = {
        if (!file.exists()) new FileNotFoundException(file.getName)
        assume(file.isDirectory, file.getName + " is not a directory.")
        file.listFiles()
    }

    def getText(file: File): String = {
        try {
            Source.fromFile(file).getLines.mkString("\n")
        } catch {
            case e: Throwable => throw e
        }
    }

    def withMakeParentDirs(path: String): File = withMakeParentDirs(new File(path))

    def withMakeParentDirs(file: File): File = {
        file.getParent match {
            case null =>
            case p => new File(p).mkdirs()
        }
        file
    }

    def getPrintStream(path: String): PrintStream = getPrintStream(new File(path))

    def getPrintStream(file: File): PrintStream = new PrintStream(withMakeParentDirs(file))

    def write(pstream: PrintStream)(op: PrintWriter => Unit): Unit = {
        val writer = new PrintWriter(pstream)
        try op(writer) finally writer.close()
    }
}