package util

import java.io.File

import config.SentenceUnitReconstructorConfig
import pas.Doc
import util.string.{NormalizedStringOption, NormalizedStringSome, StringSome}

/**
  * Created by nakayama.
  */
trait Cache {
    final val cacheEnable: Boolean = SentenceUnitReconstructorConfig.cacheIsEnable

    protected[this] val docsCacheFileName: String

    final private[this] lazy val docsCacheFile: Option[File] =
        SentenceUnitReconstructorConfig.cacheDir match {
            case Some(d) => Some(new File(s"${d}/${docsCacheFileName}"))
            case _ => None
        }

    private var docsCache: Option[Map[String, Doc]] = None

    def loadDocsCache(): Unit = docsCacheFile match {
        case Some(f) if f.exists =>
            System.err.println(s"loading cache from ${f.getAbsolutePath} ...")
            docsCache = Some(Serializer.load[Map[String, Doc]](f))
        case _ =>
    }

    // Initialize cache
    loadDocsCache()

    def getDocCache(docText: String): Option[Doc] = docsCache match {
        case Some(c) => c.get(docText)
        case _ => None
    }

    def getDocCache(normalizedStringOption: NormalizedStringOption): Option[Doc] =
        normalizedStringOption match {
            case NormalizedStringSome(ns) => ns.toStringOption match {
                case StringSome(s) => docsCache match {
                    case Some(c) => c.get(s)
                    case _ => None
                }
                case _ => None
            }
            case _ => None
        }

    def saveDocsCache(docs: Seq[Doc]): Unit =
        docsCacheFile match {
            case Some(f) =>
                System.err.println(s"saving cache to ${f.getAbsolutePath} ...")
                Serializer.save(docsCache.getOrElse(Map.empty[String, Doc]) ++ docs.map(d => (d.text, d)), f)
            case _ => throw new RuntimeException("Cache directory setting was not found.")
        }
}

trait ChaPASCache extends Cache {
    override final protected[this] val docsCacheFileName: String = "chapas-docs"
}

trait ShowcaseCache extends Cache {
    override final protected[this] val docsCacheFileName: String = "showcase-docs"
}