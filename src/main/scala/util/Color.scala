package util

import pas.{Chunk, PAS, Sentence, Token}

/**
  * Created by nakayama.
  */
object Color {
    final val RESET = "\u001B[0m"
    final val UNDERLINE = "\u001B[4m"
    final val BLACK = "\u001B[30m"
    final val RED = "\u001B[31m"
    final val GREEN = "\u001B[32m"
    final val YELLOW = "\u001B[33m"
    final val BLUE = "\u001B[34m"
    final val PURPLE = "\u001B[35m"
    final val CYAN = "\u001B[36m"
    final val WHITE = "\u001B[37m"

    def coloring(sentence: Sentence): String = sentence.chunks.flatMap(c => coloring(c)).mkString

    def coloring(chunk: Chunk): String = coloring(chunk.tokens)

    def coloring(tokens: Seq[Token]): String =
        tokens.map { t =>
            val s = new StringBuilder()
            if (t.isPredicate) {
                s.append(BLUE)
            }
            if (t.argId.isInstanceOf[Some[Int]]) {
                s.append(UNDERLINE)
            }
            s.append(t).append(RESET).result()
        }.mkString

    def coloring(chunk: Chunk, pas: PAS): String = coloring(chunk.tokens, pas)

    def coloring(tokens: Seq[Token], pas: PAS): String = tokens.map(coloring(_, pas)).mkString

    def coloring(token: Token, pas: PAS): String = {
        val s = new StringBuilder()
        if (token == pas.predicate) {
            s.append(BLUE)
        }
        if (pas.arguments.values.exists(_ == token)) {
            s.append(UNDERLINE)
        }
        s.append(token).append(RESET).result()
    }

}