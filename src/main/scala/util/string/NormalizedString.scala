package util.string

import java.text.{Normalizer => JavaNormalizer}

object NormalizedString {
    def apply(str: StringOption): NormalizedString = new NormalizedString(str)
}

class NormalizedString(str: StringOption) {
    val normalizedString: StringOption = normalize

    private def normalize: StringOption = Normalizer.normalize(str)

    override def toString: String = normalizedString.getOrElse("")

    def toStringOption: StringOption = normalizedString

    override def hashCode: Int = this.toString.toInt

    override def equals(other: Any): Boolean = other match {
        case that: NormalizedString =>
            (that canEqual this) && (this.toString == that.toString)
        case _ => false
    }

    def canEqual(other: Any): Boolean = other.isInstanceOf[NormalizedString]

    def concat(nStr: NormalizedString): NormalizedString =
        new NormalizedString(normalizedString.map(_.concat(nStr.toString)))

    def split(regex: String): Array[NormalizedString] = normalizedString match {
        case StringSome(nStr) =>
            nStr.split(regex).map(t => new NormalizedString(StringOption(t)))
        case StringNone => Array[NormalizedString]()
    }

    def split(separator: Char): Array[NormalizedString] = {
        Normalizer.normalize(StringOption(separator.toString)) match {
            case StringSome(nSeparator) =>
                normalizedString match {
                    case StringSome(nStr) =>
                        nStr.split(nSeparator.head).map(t => new NormalizedString(StringOption(t)))
                    case StringNone => Array[NormalizedString]()
                }
            case StringNone => Array[NormalizedString]()
        }
    }

    def trim: NormalizedString = {
        normalizedString.map(_.trim)
        this
    }
}

object Normalizer {
    def normalize(str: StringOption): StringOption = {
        str match {
            case StringSome(s) =>
                StringOption(JavaNormalizer.normalize(s.
                    replaceAll("\u003D", "\uFF1D"). //= ＝
                    replaceAll("\u301C", "\uFF5E"). //〜 ～
                    replaceAll("\u007E", "\uFF5E"), //~ ～
                    JavaNormalizer.Form.NFKC))
            case StringNone => StringNone
        }
    }
}

class NormalizedStringBuilder {
    val builder = new StringBuilder

    def append(nStr: NormalizedString): NormalizedStringBuilder = {
        builder.append(nStr.toString)
        this
    }

    def result(): NormalizedString =
        new NormalizedString(StringOption(builder.toString()))

    def clear(): Unit = builder.clear()
}

class NormalizedStringBuffer {
    val buffer = new StringBuffer

    def append(nStr: NormalizedString): NormalizedStringBuffer = {
        buffer.append(nStr.toString)
        this
    }

    def result(): NormalizedString =
        new NormalizedString(StringOption(buffer.toString))

    def clear(): Unit = buffer.delete(0, buffer.length())
}