package reader

import util.string.{NormalizedString, StringOption}

trait RawStringParser {
    def parse(text: StringOption): Seq[NormalizedString]
}