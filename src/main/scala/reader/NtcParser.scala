package reader

import java.io.File

import pas.Doc

import scala.io.Source

/**
  * Created by nakayama.
  */
abstract class NtcParser {
    def parse(ntcFile: File): Doc = parse(Source.fromFile(ntcFile).getLines())

    def parse(ntcText: Iterator[String]): Doc
}
