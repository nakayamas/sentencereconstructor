package reader

import util.string._

import scala.collection.mutable.ListBuffer

object RawSentenceParser extends RawStringParser {
    private val japanesePeriodRegex = "[。．]"
    private val japaneseCommaRegex = "[、，]"
    private val japanesePeriod = "。"
    private val japaneseComma = "、"

    override def parse(text: StringOption): Seq[NormalizedString] = {
        text match {
            case StringSome(t) =>
                val sentences = ListBuffer[NormalizedString]()

                // 改行文字により行に分割
                t.split('\n').foreach { line =>
                    sentences ++= {
                        // 句点により文単位に分割
                        line.trim.split(japanesePeriodRegex).collect {
                            case s if StringOption(s).nonEmpty => normalize(s)
                        }
                    }
                }

                sentences.result()
            case StringNone => Nil
        }
    }

    private def normalize(sentence: String): NormalizedString = {
        val phrases = new StringBuilder()

        // 読点により節に分割
        sentence.trim.split(japaneseCommaRegex).foreach { clause =>
            Normalizer.normalize(StringOption(clause.trim)) match {
                case StringSome(p) =>
                    phrases
                        // 節を NFKC 正規化
                        .append(p)
                        // 節末に読点を追加
                        .append(japaneseComma)
                case StringNone =>
            }
        }
        new NormalizedString(StringOption {
            if (phrases.nonEmpty) {
                phrases
                    // 文末の読点を削除
                    .deleteCharAt(phrases.size - 1)
                    // 文末に句点を追加
                    .append(japanesePeriod)
            }
            phrases.result()
        })
    }
}