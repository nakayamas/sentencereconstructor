package reader

import pas._

import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
object ShowcaseNtcParser extends NtcParser {
    def parse(ntcText: Iterator[String]): Doc = {
        //    val sentRegExp = """^\# S-ID:(.+) .*""".r
        val chunkRegExp =
            """^\* (\d+) (-?\d+)([A-Z]) .*""".r
        val tokenRegExp = """^([^\t]+)\t([^\t]+)\t([^\t]+)(\s*)(.*)""".r
        val pasRegExp = """^([^=]+)="(.+)"$""".r
        val blankRegExp = """\s*""".r

        var doc: Doc = null
        var sentences: ArrayBuffer[Sentence] = ArrayBuffer[Sentence]()
        var chunks: ArrayBuffer[Chunk] = ArrayBuffer[Chunk]()
        var tokens: ArrayBuffer[Token] = ArrayBuffer[Token]()

        var currentChunkId: Int = 0
        var currentHeadId: Int = 0
        var currentDepType: DepType.Value = null

        val sentenceId: Iterator[Int] = Stream.from(0).iterator
        var tokenId: Iterator[Int] = Stream.from(0).iterator
        ntcText.foreach {
            case "EOD" =>
                doc = Doc(sentences)
                doc.sentences.foreach { s =>
                    s.doc = doc
                    s.chunks.foreach { c =>
                        c.doc = doc
                        c.tokens.foreach(_.doc = doc)
                    }
                }
            case "EOS" =>
                if (tokens.nonEmpty) {
                    val chunk = Chunk(currentChunkId, tokens, currentHeadId, currentDepType)
                    chunk.tokens.foreach(_.chunk = chunk)
                    chunks += chunk

                    val sentence = Sentence(sentenceId.next, chunks)
                    sentence.chunks.foreach { c =>
                        c.sentence = sentence
                        c.tokens.foreach(_.sentence = sentence)
                    }
                    sentences += sentence
                }
                chunks = ArrayBuffer[Chunk]()
                tokens = ArrayBuffer[Token]()
                tokenId = Stream.from(0).iterator
            case chunkRegExp(id, headId, depType) =>
                if (tokens.nonEmpty) {
                    val chunk = Chunk(currentChunkId, tokens, currentHeadId, currentDepType)
                    chunk.tokens.foreach(_.chunk = chunk)
                    chunks += chunk
                }

                currentChunkId = id.toInt
                tokens = ArrayBuffer[Token]()
                currentHeadId = headId.toInt
                currentDepType = depType match {
                    case "P" => DepType.P
                    case "A" => DepType.A
                    case "I" => DepType.I
                    case _ => DepType.D
                }
            case tokenRegExp(surface, posLine, bio, _, pasLine) =>
                val pasMap = pasLine match {
                    case blankRegExp() => None
                    case _ =>
                        Some(pasLine.split(" ").collect {
                            case pasRegExp(k, v) => (k, v)
                        }.toMap)
                }
                val posList = posLine.split(",")
                val base = posList(4)
                val pos = posList(0)
                val detailedPos = posList(1) match {
                    case "*" => None
                    case p => Some(p)
                }
                val semantic = posList(6) match {
                    case "*" => None
                    case s => Some(s)
                }
                tokens += new Token(tokenId.next, surface, base, pos, detailedPos, semantic, pasMap)
            case l => new IllegalStateException("Unexpected format: " + l)
        }
        doc
    }
}