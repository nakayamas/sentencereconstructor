package reader

import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import util.string._

/**
  * Created by nakayama.
  */
class RawSentenceParserTest extends FlatSpec with Matchers with PrivateMethodTester {
    "StringOption" should "have expected value" in {
        StringOption("test") shouldBe StringSome("test")
        StringOption("\n") shouldBe StringSome("\n")
        StringOption("") shouldBe StringNone
        StringOption(null) shouldBe StringNone
    }

    "NormalizedString" should "normalize given strings" in {
        NormalizedString(StringOption("日本語")).normalizedString shouldBe StringSome("日本語")
        NormalizedString(StringOption("日本語")).toString shouldBe "日本語"
        // NormalizedString(StringOption("イコール=いこーる")).toString shouldBe "イコール＝いこーる"
        // NormalizedString(StringOption("波線〜なみせん")).toString shouldBe "波線～なみせん"
        // NormalizedString(StringOption("チルダ~ちるだ")).toString shouldBe "チルダ～ちるだ"
    }

    "NormalizedStringOption" should "behave like Option" in {
        NormalizedStringOption(NormalizedString(StringOption("test"))).get.toString shouldBe
            NormalizedStringSome(NormalizedString(StringOption("test"))).get.toString
        NormalizedStringOption(NormalizedString(StringNone)) shouldBe NormalizedStringNone
    }

    "RawSentenceParser" should "split into sentences and normalize" in {
        RawSentenceParser.parse(StringOption("あ")).head.toString shouldBe "あ。"
        RawSentenceParser.parse(StringOption("、")).foreach(s => println(s"[${s}]"))
        RawSentenceParser.parse(StringOption("、")).isEmpty shouldBe true
        RawSentenceParser.parse(StringOption("，")).head.toStringOption shouldBe StringNone
        RawSentenceParser.parse(StringOption("。")).head.toString shouldBe "。"
        RawSentenceParser.parse(StringOption("．")).head.toString shouldBe "。"

        val normalizeMethod: PrivateMethod[NormalizedString] = PrivateMethod[NormalizedString]('normalize)
        val normalizedSentence0: NormalizedString = RawSentenceParser invokePrivate normalizeMethod("今日は、いい天気だ")
        normalizedSentence0.toString shouldBe "今日は、いい天気だ。"
        val normalizedSentence1: NormalizedString = RawSentenceParser invokePrivate normalizeMethod("今日は，いい天気だ")
        normalizedSentence1.toString shouldBe "今日は、いい天気だ。"
        val normalizedSentence2: NormalizedString = RawSentenceParser invokePrivate normalizeMethod("今日は，とても，いい天気だ")
        normalizedSentence2.toString shouldBe "今日は、とても、いい天気だ。"

        val correctText: Seq[NormalizedString] = Seq(
            NormalizedString(StringOption("今日は、いい天気だ。")),
            NormalizedString(StringOption("明日も晴れるだろう。")),
            NormalizedString(StringOption("来週はどうかわからない。")))
        val normalizedText0: Seq[NormalizedString] = RawSentenceParser.parse(StringOption(correctText.flatMap(_.toString).mkString))
        normalizedText0 should contain theSameElementsInOrderAs correctText
        val normalizedText1 = RawSentenceParser.parse(StringOption("今日は、いい天気だ．明日も晴れるだろう。\n来週はどうかわからない。"))
        normalizedText1 should contain theSameElementsInOrderAs correctText
        val normalizedText2 = RawSentenceParser.parse(StringOption("今日は、いい天気だ。明日も晴れるだろう\n来週はどうかわからない"))
        normalizedText2 should contain theSameElementsInOrderAs correctText
    }
}