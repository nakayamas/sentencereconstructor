package reader

import java.io.File

import org.scalatest.{FlatSpec, Matchers}
import pas.{DepType, Doc}

/**
  * Created by nakayama.
  */
class ChaPASNtcParserTest extends FlatSpec with Matchers {

    val ntcFile: File = new File("src/test/resources/chapas_sample_doc.ntc")
    val doc: Doc = ChaPASNtcParser.parse(ntcFile)

    "Doc" should "have PASes" in {
        doc.pases.length shouldBe 4
    }

    "Member values and variables of each Sentence" should "have expected values" in {
        doc.sentences.foreach(_.doc shouldBe doc)
        doc.sentences.head.text shouldBe {
            "ウェストファリア条約で帝国内の領邦がほぼ完全な主権を認められたことにより、一つの帝国としての機能は失われた。"
        }
        doc.sentences.head.tokens.mkString shouldBe {
            "ウェストファリア条約で帝国内の領邦がほぼ完全な主権を認められたことにより、一つの帝国としての機能は失われた。"
        }
    }

    "Member values and variables of each Chunk" should "have expected values" in {
        for {
            sentence <- doc.sentences
            chunk <- sentence.chunks
        } {
            chunk.doc shouldBe doc
            chunk.sentence shouldBe sentence
        }

        val sentence0 = doc.sentences.head
        val chunks0 = sentence0.chunks
        chunks0.head.text shouldBe "ウェストファリア条約で"
        chunks0(1).text shouldBe "帝国内の"
        chunks0.tail.last.text shouldBe "失われた。"
        chunks0(6).textWithBase shouldBe "認めるられるた"
        chunks0.tail.last.textWithBase shouldBe "失うれるた。"

        chunks0.head.depType shouldBe DepType.D
        chunks0.head.hasPredicates shouldBe false
        chunks0(6).hasPredicates shouldBe true

        sentence0.pases.map(_.predicate.chunk.text) should contain theSameElementsAs Seq("完全な", "認められた", "機能は", "失われた。")

        chunks0.head.depDest shouldBe Some(chunks0(6))
        chunks0(1).depDest shouldBe Some(chunks0(2))
        chunks0.last.depDest shouldBe None

        chunks0.head.depSrcs.length shouldBe 0
        chunks0(6).depSrcs should contain theSameElementsAs Seq(chunks0.head, chunks0(2), chunks0(5))
        chunks0.last.depSrcs should contain theSameElementsAs Seq(chunks0(7), chunks0(10))
    }

    "Member values and variables of each Token" should "have expected values" in {
        for {
            sentence <- doc.sentences
            chunk <- sentence.chunks
            token <- chunk.tokens
        } {
            token.doc shouldBe doc
            token.sentence shouldBe sentence
            token.chunk shouldBe chunk
        }
    }

    "Member values and variables of the second token in the second sentence" should "have expected values" in {
        val token1 = doc.sentences.head.chunks.head.tokens(1)
        token1.id shouldBe 1
        token1.surface shouldBe "条約"
        token1.base shouldBe "条約"
        token1.pos shouldBe "名詞"

        token1.detailedPos shouldBe Some("一般")
        token1.semantic shouldBe None
        token1.pas shouldBe None
        token1.isPredicate shouldBe false
    }

    "Member values and variables of the 20th token in the second sentence" should "have expected values" in {
        val token14 = doc.sentences.head.tokens(14)
        token14.id shouldBe 14
        token14.surface shouldBe "認め"
        token14.base shouldBe "認める"
        token14.pos shouldBe "動詞"
        token14.detailedPos shouldBe Some("自立")
        token14.semantic shouldBe None
        token14.pas shouldBe a[Some[_]]
        token14.isPredicate shouldBe true
    }

}