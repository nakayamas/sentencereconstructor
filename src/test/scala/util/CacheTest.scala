package util

import java.io.File

import config.SentenceUnitReconstructorConfig
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import pas.Doc
import pas.parser.ChaPAS
import reader.{ChaPASNtcParser, RawSentenceParser}
import util.string.{NormalizedString, NormalizedStringOption, StringOption}

import scala.io.Source

/**
  * Created by nakayama.
  */
class CacheTest extends FlatSpec with Matchers with PrivateMethodTester {
    val sampleFile = new File("src/test/resources/sample_doc.txt")

    val originalText = Source.fromFile(sampleFile).mkString("\n")
    val normalizedSentences: Seq[NormalizedString] = RawSentenceParser.parse(StringOption(originalText))

    val chapasDoc0: Doc = ChaPASNtcParser.parse(ChaPAS.analyze(NormalizedStringOption(normalizedSentences.head)).iterator)
    chapasDoc0.sentences.foreach { s =>
        val normalizedString: NormalizedStringOption = NormalizedStringOption(NormalizedString(StringOption(s.text)))
        s.depTree = ChaPAS.cabochaTree(normalizedString).mkString("\n").stripLineEnd
    }

    "Cached ChaPAS doc" should "be saved correctly" in {
        ChaPAS.saveDocsCache(Seq(chapasDoc0))
        new File(SentenceUnitReconstructorConfig.cacheDir.get + "/chapas-docs") should exist
    }

    it should "equals to non-cached doc" in {
        ChaPAS.getDocCache(NormalizedStringOption(normalizedSentences.head)) shouldBe a[Some[_]]

        val docByCache: Doc = ChaPAS.getDocCache(NormalizedStringOption(normalizedSentences.head)).get
        chapasDoc0.text shouldBe docByCache.text
    }
}