package parser

import org.scalatest.{FlatSpec, Matchers}
import pas.parser.CaboCha
import util.string.{NormalizedString, NormalizedStringOption, StringOption}

import scala.io.Source

/**
  * Created by nakayama.
  */
class CaboChaTest extends FlatSpec with Matchers {
    val text: String = Source.fromFile("src/test/resources/sample_doc.txt").mkString

    "cabocha" should "parse correctly" in {
        CaboCha.cabochaTreeIPACmd(NormalizedStringOption(NormalizedString(StringOption(text)))).!! should be {
            Source.fromFile("src/test/resources/sample_doc-cabocha_tree_ipa.txt").mkString
        }
        CaboCha.cabochaLatticeIPACmd(NormalizedStringOption(NormalizedString(StringOption(text)))).!! should be {
            Source.fromFile("src/test/resources/sample_doc-cabocha_lattice_ipa.txt").mkString
        }
        CaboCha.cabochaTreeJumanCmd(NormalizedStringOption(NormalizedString(StringOption(text)))).!! should be {
            Source.fromFile("src/test/resources/sample_doc-cabocha_tree_juman.txt").mkString
        }
        CaboCha.cabochaLatticeJumanCmd(NormalizedStringOption(NormalizedString(StringOption(text)))).!! should be {
            Source.fromFile("src/test/resources/sample_doc-cabocha_lattice_juman.txt").mkString
        }
    }
}