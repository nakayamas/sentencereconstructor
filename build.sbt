name := "SentenceReconstructor"

version := "0.4.5"

scalaVersion := "2.11.7"

scalacOptions ++= Seq(
    "-deprecation",
    "-feature",
    "-unchecked"
)

resolvers ++= Seq(
    "Sonatype OSS Releases"  at "http://oss.sonatype.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
    "commons-cli" % "commons-cli" % "1.3.1" withSources() withJavadoc(),
    "com.typesafe" % "config" % "1.3.0" withSources() withJavadoc(),
    "net.ceedubs" %% "ficus" % "1.1.2" withSources() withJavadoc(),
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test" withSources() withJavadoc()
)
