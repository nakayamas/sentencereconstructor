#!/usr/bin/env bash
set -e

working_dir="$(pwd)"

cd "$(dirname ${0})/../.."

config="script/runner.conf"

#sbt "run-main reconstructor.SentenceUnitReconstructor -c \"${config}\" -o \"result.$(date -I)/sample_chapas.txt\""
sbt "run-main reconstructor.SentenceUnitReconstructorDemo -c \"${config}\" -color -o \"result.$(date -I)/sample_chapas_color.txt\""