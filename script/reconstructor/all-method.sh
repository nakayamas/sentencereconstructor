#!/usr/bin/env bash
set -e

working_dir="$(pwd)"

cd "$(dirname ${0})/../.."

config="script/runner.conf"

sbt "run-main reconstructor.SentenceUnitReconstructor -c \"${config}\" -chapas -o result/sample_chapas.txt"
sbt "run-main reconstructor.SentenceUnitReconstructor -c \"${config}\" -chapas -color -o result/sample_chapas_color.txt"

sbt "run-main reconstructor.SentenceUnitReconstructor -c \"${config}\" -showcase -o result/sample_showcase.txt"
sbt "run-main reconstructor.SentenceUnitReconstructor -c \"${config}\" -showcase -color -o result/sample_showcase_color.txt"